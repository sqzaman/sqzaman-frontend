import React from 'react';
import '@babel/polyfill';
import {
  cleanup, render, fireEvent, getByText, getByPlaceholderText,
  queryAllByText,
} from 'react-testing-library';
import 'jest-dom/extend-expect';
import Questionnaire from '../src/Questionnaire';
import { getByTestId } from 'dom-testing-library'

describe('Questionnaire Test Suites', () => {
  const setup = () => render(<Questionnaire />).container;

  afterEach(cleanup);

  it('should add a text question when Add Text Question button is clicked', async () => {
    const container = setup();
    const addTextQuestionBtn = getByText(container, 'Add A Text Question');

    expect(addTextQuestionBtn).toBeVisible();

    fireEvent.click(addTextQuestionBtn);
    const saveButton = getByText(container, 'Save Question');
    const input = getByPlaceholderText(container, 'Enter a question text...');
    expect(saveButton).toBeVisible();
    expect(input).toBeVisible();
  });

  it('should save the text question when Save button is clicked', () => {
    const container = setup();
    const addTextQuestionBtn = getByText(container, 'Add A Text Question');

    fireEvent.click(addTextQuestionBtn);

    const saveButton = getByText(container, 'Save Question');
    const input = getByPlaceholderText(container, 'Enter a question text...');
    const questionText = 'What is your age?';

    fireEvent.change(input, { target: { value: questionText } });
    fireEvent.click(saveButton);

    const editButton = getByText(container, 'Edit Question');
    const questionTextH4 = container.querySelector('h4');

    expect(editButton).toBeVisible();
    expect(questionTextH4).toHaveTextContent(questionText);
  });

  it('should edit the text question when Edit button is clicked', () => {
    const container = setup();
    const addTextQuestionBtn = getByText(container, 'Add A Text Question');

    fireEvent.click(addTextQuestionBtn);

    const saveButton = getByText(container, 'Save Question');
    const questionText = 'What is your age?';

    const input = getByPlaceholderText(container, 'Enter a question text...');
    fireEvent.change(input, { target: { value: questionText } });

    fireEvent.click(saveButton);
    const editButton = getByText(container, 'Edit Question');

    fireEvent.click(editButton);
    expect(input).toBeVisible();
    expect(input.value).toBe(questionText);
  });

  it('should delete the text question when Delete button is clicked', () => {
    const container = setup();
    const addTextQuestionBtn = getByText(container, 'Add A Text Question');
    fireEvent.click(addTextQuestionBtn);

    const saveButton = getByText(container, 'Save Question');
    const questionText = 'What is your age?';

    const input = getByPlaceholderText(container, 'Enter a question text...');
    fireEvent.change(input, { target: { value: questionText } });

    fireEvent.click(saveButton);
    const deleteButton = getByText(container, 'Delete Question');

    fireEvent.click(deleteButton);

    const allDeleteButtons = queryAllByText(container, 'Delete Question');
    expect(allDeleteButtons.length).toBe(0);
  });

  it('should add a multiselect question when Add Multiselect Question button is clicked', async () => {
    const container = setup();
    const addMultiSelectQuestionBtn = getByText(container, 'Add A Multi Select Question');

    expect(addMultiSelectQuestionBtn).toBeVisible();

    fireEvent.click(addMultiSelectQuestionBtn);
    const saveButton = getByText(container, 'Save Question');
    const deleteButton = getByText(container, 'Delete Question');
    const addOptionButton = getByText(container, 'Add Option');
    const input = getByPlaceholderText(container, 'Enter a question text...');

    expect(saveButton).toBeVisible();
    expect(deleteButton).toBeVisible();
    expect(addOptionButton).toBeVisible();
    expect(input).toBeVisible();
  });

  it('should add a option to multiselect question when Add Option button is clicked', async () => {
    const container = setup();
    const addMultiSelectQuestionBtn = getByText(container, 'Add A Multi Select Question');

    expect(addMultiSelectQuestionBtn).toBeVisible();

    fireEvent.click(addMultiSelectQuestionBtn);

    const addOptionButton = getByText(container, 'Add Option');
    expect(addOptionButton).toBeVisible();   

    fireEvent.click(addOptionButton);

    const saveOption = getByText(container, 'Save Option');
    const deleteOption = getByText(container, 'Delete Option');
    const input = getByPlaceholderText(container, 'Enter a option text...');
    expect(saveOption).toBeVisible();
    expect(deleteOption).toBeVisible();
    expect(input).toBeVisible();

  });

  it('should save the option and test up/down/delete of a multiselect question when individual Option is clicked', () => {
    const container = setup();
    const addMultiSelectQuestionBtn = getByText(container, 'Add A Multi Select Question');

    expect(addMultiSelectQuestionBtn).toBeVisible();

    fireEvent.click(addMultiSelectQuestionBtn);

    const addOptionButton = getByText(container, 'Add Option');
    expect(addOptionButton).toBeVisible();   

    fireEvent.click(addOptionButton);

    const saveOption = getByText(container, 'Save Option');
    const input = getByPlaceholderText(container, 'Enter a option text...');
    const optionText = 'Yes';

    fireEvent.change(input, { target: { value: optionText } });
    fireEvent.click(saveOption);
    const editOption = getByText(container, 'Edit Option');
    const optionTextH4One = container.querySelector('h4');

    expect(editOption).toBeVisible();
    expect(optionTextH4One).toHaveTextContent(optionText);

    
    // add 2nd option
    fireEvent.click(addOptionButton);
    const saveOptionTwo = getByText(container, 'Save Option');
    const inputTwo = getByPlaceholderText(container, 'Enter a option text...');
    const optionTextTwo = 'No';

    fireEvent.change(inputTwo, { target: { value: optionTextTwo } });
    fireEvent.click(saveOptionTwo);
    const editOptionTwo = getByText(container, 'Edit Option');
    const optionTextH4Two = container.querySelector('h4');

    expect(editOptionTwo).toBeVisible();
    expect(optionTextH4Two).not.toBe(optionTextTwo);

    const upButton = getByText(container, 'Up');
    const downButton = getByText(container, 'Down');
    const deleteOption = getByText(container, 'Delete Option');
    expect(upButton).toBeVisible();
    expect(downButton).toBeVisible();
    expect(deleteOption).toBeVisible();

    // test up button
    fireEvent.click(upButton);
    expect(container.querySelector('h4')).toHaveTextContent(optionTextTwo);

    // test down button
    fireEvent.click(downButton);
    expect(container.querySelector('h4')).not.toBe(optionTextTwo);
    expect(container.querySelector('h4')).toHaveTextContent(optionTextTwo);

    // test delete option
    fireEvent.click(deleteOption);
    expect(deleteOption).toBeVisible();





  });

});
